package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me (done)
        this.guild = guild;
    }

    //ToDo: Complete Me (done)
    public void update(){
        if(!guild.getQuestType().equals("Rumble")){
            this.getQuests().add(guild.getQuest());
        }
    }
}
