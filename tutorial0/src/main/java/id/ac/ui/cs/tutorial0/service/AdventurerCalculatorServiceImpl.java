package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {
    private int power = 0;

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            rawAge *= 2000;
        } else if (rawAge <50) {
            rawAge *= 2250;
        } else {
            rawAge *= 5000;
        }
        this.power = rawAge;
        return rawAge;
    }

    public String powerClassType(){
        if(this.power <= 20000){
            return "C class";
        }
        else if(this.power <= 100000){
            return "B class";
        }
        else{
            return "A class";
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
